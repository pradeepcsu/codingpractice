"""main file to process words from text file"""
import argparse
import os
import pathlib
import sys
import unicodedata

# using pathlib for file path handling
PACKAGE_DIR = pathlib.Path(__file__).parent


class WordProcessor:
    """
    Capable of processing words from a file or from a list and keeping track of longest words found
    """
    @staticmethod
    def read_words_from_file(filepath):
        """Reads a file at ``filepath`` and yields all valid words from file.
        Using this generator to prevent loading the entire file into memory
        """
        if not filepath.endswith('.txt'):
            raise argparse.ArgumentTypeError("Only .txt files allowed")
        if os.stat(filepath).st_size == 0:
            raise ValueError("file is empty: {filepath}")
        with open(filepath, 'r', encoding='utf-8') as fin:
            for line in fin:
                line = line.strip('\n')  # remove newline
                words = line.split()
                for word in words:
                    yield word

    @staticmethod
    def file_type_check(path):
        """
        checks the file type given by user and throws error if file type is not text
        """
        if not path.endswith(".txt"):
            raise argparse.ArgumentTypeError("Only .txt files allowed")
        return path

    def __init__(self):
        self.words = []
        self.word_length = 0
        self.lang_in_word = set()

    def clear(self):
        """Resets the state of the processor."""
        self.words.clear()
        self.word_length = 0

    def process_word(self, word):
        """Process each word from file"""
        # if not word.isalnum():
        #     print("skipping line: {word}")
        #     return
        if len(word) < self.word_length:
            return
        if len(word) == self.word_length:
            self.words.append(word)
            return
        self.words.clear()
        self.words.append(word)
        self.word_length = len(word)

    def process_file(self, filepath):
        """Reads a file at ``filepath`` and returns word(s) """
        print("processing file: {filepath}")
        for word in self.read_words_from_file(filepath):
            self.process_word(word)

    def print_words(self, formatter=None):
        """Display words."""
        if not self.words:
            print("no words found")
            return
        for word in self.words:
            if formatter:
                word = formatter(word)
            print(word)
            print(self.get_languages_used(word))

    def get_languages_used(self, word):
        """
        This method loops through the word and gives the list of languages and special characters used in the word 
        """
        word_processed = list((map(unicodedata.name, word)))
        for char in word_processed:
            self.lang_in_word.add(char)
        return self.lang_in_word

def main():
    """Main program entry point.
       For demo purpose I provided default file file_with_words.txt
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--filepath',
                        default=PACKAGE_DIR/'data'/'file_with_words.txt', help="text file to read")
    args = parser.parse_args()
    _args = vars(args)

    processor = WordProcessor()
    processor.process_file(_args['filepath'])

    print("longest word(s):")
    processor.print_words()
    print("transposed longest word(s):")
    processor.print_words(formatter=lambda word: word[::-1])

    return 0


if __name__ == '__main__':
    sys.exit(main())



#==================== Test text file ==================
# def file_type_check(path):
#     """
#     checks the file type given by user and throws error if file type is not text
#     """
#     if not path.endswith(".txt"):
#         raise argparse.ArgumentTypeError("Only .txt files allowed")
#     return path

# parser = argparse.ArgumentParser()
# parser.add_argument('--foo', help='foo help', type=file_type_check)

# wordstring = 'he$(****コПрив好'
# word = list((map(unicodedata.name, wordstring)))

# langs= set()
# # for i in word:
# #     print('i is:'+ i)
# #     j = i.split()
# #     langs.add(j[0])
# #     print('j is:'+ j[0])

# for i in word:
#     print('i is:'+ i)
#     langs.add(i)

# # print(word)
# print(langs)
